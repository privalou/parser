package tech.picnic.assignment.impl.util;

import com.fasterxml.jackson.databind.util.StdDateFormat;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.util.Date;

@Slf4j
public final class DateUtil {

    private DateUtil() {
    }

    public static Date parseDate(String date) {
        try {
            return new StdDateFormat().parse(date);
        } catch (ParseException e) {
            log.error("Could not parse date string in ISO 8601 format.");
            throw new RuntimeException("Could not parse date string in ISO 8601 format.", e);
        }
    }
}
