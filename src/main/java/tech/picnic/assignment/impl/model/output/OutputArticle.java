package tech.picnic.assignment.impl.model.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import tech.picnic.assignment.impl.util.DateUtil;

@Data
@Builder
public class OutputArticle implements Comparable<OutputArticle> {

    @JsonProperty("article_name")
    private String articleName;

    private String timestamp;

    @Override
    public int compareTo(OutputArticle o) {
        return DateUtil.parseDate(timestamp).compareTo(DateUtil.parseDate(o.getTimestamp()));
    }
}
