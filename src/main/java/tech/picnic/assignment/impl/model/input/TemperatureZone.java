package tech.picnic.assignment.impl.model.input;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;

public enum TemperatureZone implements Serializable {

    AMBIENT("ambient"),
    CHILLED("chilled");

    private final String name;

    TemperatureZone(String name) {
        this.name = name;
    }

    @JsonValue
    public String getName() {
        return name;
    }
}
