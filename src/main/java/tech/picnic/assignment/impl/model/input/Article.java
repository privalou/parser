package tech.picnic.assignment.impl.model.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Article {

    private String id;

    private String name;

    @JsonProperty("temperature_zone")
    private TemperatureZone temperatureZone;
}
