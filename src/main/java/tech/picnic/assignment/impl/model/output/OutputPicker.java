package tech.picnic.assignment.impl.model.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import tech.picnic.assignment.impl.util.DateUtil;

import java.util.Set;

@Builder
@Data
public class OutputPicker implements Comparable<OutputPicker> {

    @JsonProperty("picker_name")
    private String pickerName;

    @JsonProperty("active_since")
    private String activeSince;

    private Set<OutputArticle> picks;

    @Override
    public int compareTo(OutputPicker o) {
        return DateUtil.parseDate(activeSince).compareTo(DateUtil.parseDate(o.getActiveSince()));
    }
}
