package tech.picnic.assignment.impl.model.input;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Picker {

    private String id;

    private String name;

    @JsonProperty("active_since")
    private String activeSince;
}
