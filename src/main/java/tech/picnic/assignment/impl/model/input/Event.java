package tech.picnic.assignment.impl.model.input;

import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
public class Event {

    private String id;

    private String timestamp;

    private Picker picker;

    private Article article;

    private Integer quantity;
}
