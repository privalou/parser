package tech.picnic.assignment.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.RequiredArgsConstructor;


@Getter
@RequiredArgsConstructor
public class ParserConfig {
    private final JsonParser jsonParser;
    private final ObjectMapper objectMapper;
}

