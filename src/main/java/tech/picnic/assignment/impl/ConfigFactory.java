package tech.picnic.assignment.impl;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;

@Slf4j
public final class ConfigFactory {

    private ConfigFactory() {
    }

    public static ParserConfig createConfig(InputStream inputStream) {

        ObjectMapper objectMapper = new ObjectMapper();

        JsonFactory jsonFactory = objectMapper.getFactory();
        try {
            JsonParser parser = jsonFactory.createParser(inputStream);
            return new ParserConfig(parser, objectMapper);
        } catch (IOException e) {
            log.error("Error occurred during json factory initialization");
        }

        return null;
    }
}
