package tech.picnic.assignment.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;

@Slf4j
@RequiredArgsConstructor
public class ConditionChecker {
    private final int maxEvents;
    private final Duration maxTime;
    private final long processStartTimeInNano;

    public boolean checkIfOutOfConditions(Integer eventCounter) {

        if (eventCounter >= maxEvents) {
            log.debug("Event counter reached its maximum: {}", maxEvents);
            return true;
        }

        if (System.nanoTime() - processStartTimeInNano > maxTime.toNanos()) {
            log.debug("Time is out : {} ns.", maxTime.toNanos());
            return true;
        }

        return false;
    }
}
