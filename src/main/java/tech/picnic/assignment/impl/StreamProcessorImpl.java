package tech.picnic.assignment.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import tech.picnic.assignment.api.StreamProcessor;
import tech.picnic.assignment.impl.model.input.Event;
import tech.picnic.assignment.impl.model.output.OutputPicker;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class StreamProcessorImpl implements StreamProcessor {

    private final int maxEvents;
    private final Duration maxTime;
    private final ObjectMapper objectMapper;

    public StreamProcessorImpl(int maxEvents, Duration maxTime) {
        this.maxEvents = maxEvents;
        this.maxTime = maxTime;
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public void process(InputStream source, OutputStream sink) throws IOException {
        ConditionChecker conditionChecker = new ConditionChecker(maxEvents, maxTime, System.nanoTime());
        EventPicker eventPicker = new EventPicker(ConfigFactory.createConfig(source), conditionChecker);
        OutputGenerator outputGenerator = new OutputGenerator();

        List<Event> events = new ArrayList<>();
        eventPicker.forEachRemaining(events::add);

        List<OutputPicker> outputPickerList = outputGenerator.process(events);

        write(outputPickerList, sink);
    }


    @Override
    public void close() {
    }

    private void write(List<OutputPicker> outputPickerList, OutputStream sink) {
        try {
            String prettyJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(outputPickerList);
            sink.write(prettyJson.getBytes(StandardCharsets.UTF_8));
            log.debug("Writing output to the sink.");
        } catch (IOException e) {
            log.error("Error occurred during writing output to the OutputStream sink.", e);
        }
    }
}
