package tech.picnic.assignment.impl;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import lombok.extern.slf4j.Slf4j;
import tech.picnic.assignment.impl.model.input.Event;
import tech.picnic.assignment.impl.model.input.TemperatureZone;

import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

@Slf4j
public class EventPicker implements Iterator<Event> {

    private final ParserConfig parserConfig;
    private final ConditionChecker checker;

    private boolean eventBuffered = false;
    private Integer counter;
    private Event nextEvent;

    public EventPicker(ParserConfig parserConfig, ConditionChecker checker) {
        this.parserConfig = parserConfig;
        this.checker = checker;
        this.counter = 0;
    }

    @Override
    public boolean hasNext() {
        Optional<Event> event = Optional.empty();
        if (!eventBuffered) {
            event = readEvent();
        }
        event.ifPresent(e -> {
            eventBuffered = true;
            nextEvent = e;
        });

        return event.isPresent();
    }

    @Override
    public Event next() {

        eventBuffered = false;

        Optional<Event> event = Optional.ofNullable(nextEvent);
        if (event.isEmpty()) {
            event = readEvent();
        }

        if (event.isPresent()) {
            counter++;
            return event.get();
        }

        log.debug("That are no more events");
        throw new NoSuchElementException("That are no more events");
    }

    private Optional<Event> readEvent() {

        if (checker.checkIfOutOfConditions(counter)) {
            log.debug("Reached boundary condition.");
            return Optional.empty();
        }

        try {
            JsonToken token;
            JsonParser jsonParser = parserConfig.getJsonParser();
            while ((token = jsonParser.nextToken()) != null) {
                if (token != JsonToken.START_OBJECT) {
                    continue;
                }
                Event event = parserConfig.getObjectMapper().convertValue(jsonParser.readValueAsTree(), Event.class);
                if (event.getArticle().getTemperatureZone() == TemperatureZone.AMBIENT) {
                    return Optional.of(event);
                }
            }
        } catch (IOException e) {
            log.error("Error occurred during JSON parsing.");
        }

        return Optional.empty();
    }
}
