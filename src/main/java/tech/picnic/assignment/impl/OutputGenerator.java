package tech.picnic.assignment.impl;

import tech.picnic.assignment.impl.model.input.Event;
import tech.picnic.assignment.impl.model.output.OutputArticle;
import tech.picnic.assignment.impl.model.output.OutputPicker;

import java.util.*;

public class OutputGenerator {

    private final Map<String, OutputPicker> pickerMap = new HashMap<>();

    public List<OutputPicker> process(List<Event> input) {
        input.forEach(this::obtainOutputPicker);
        ArrayList<OutputPicker> outputPickers = new ArrayList<>(pickerMap.values());
        Collections.sort(outputPickers);
        return outputPickers;
    }

    private void obtainOutputPicker(Event event) {
        OutputArticle article = OutputArticle.builder()
                .articleName(event.getArticle().getName().toUpperCase())
                .timestamp(event.getTimestamp())
                .build();

        if (pickerMap.get(event.getPicker().getName()) == null) {

            Set<OutputArticle> articles = new TreeSet<>();
            articles.add(article);

            OutputPicker picker = OutputPicker.builder()
                    .pickerName(event.getPicker().getName())
                    .activeSince(event.getPicker().getActiveSince())
                    .picks(articles)
                    .build();

            pickerMap.put(event.getPicker().getName(), picker);
        } else {
            OutputPicker picker = pickerMap.get(event.getPicker().getName());
            picker.getPicks().add(article);
        }
    }
}
