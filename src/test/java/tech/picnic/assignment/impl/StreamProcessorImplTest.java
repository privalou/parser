package tech.picnic.assignment.impl;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import tech.picnic.assignment.api.StreamProcessor;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Scanner;

public class StreamProcessorImplTest {

    private String loadResource(String resource) throws IOException {
        try (InputStream is = getClass().getResourceAsStream(resource);
             Scanner scanner = new Scanner(is, StandardCharsets.UTF_8)) {
            scanner.useDelimiter("\\A");
            return scanner.hasNext() ? scanner.next() : "";
        }
    }

    @Test
    void shouldReturnFalseAfterThreeRead() throws IOException, JSONException {

        StreamProcessor processor
                = new StreamProcessorImpl(10, Duration.ofSeconds(10));

        InputStream source = getClass().getResourceAsStream("happy-path-input-with-empty-lines.json-stream");

        ByteArrayOutputStream sink = new ByteArrayOutputStream();

        processor.process(source, sink);

        String expectedOutput = loadResource("happy-path-output.json");
        String actualOutput = new String(sink.toByteArray(), StandardCharsets.UTF_8);
        JSONAssert.assertEquals(expectedOutput, actualOutput, JSONCompareMode.STRICT);

    }
}
