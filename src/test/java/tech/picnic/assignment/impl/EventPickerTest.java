package tech.picnic.assignment.impl;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tech.picnic.assignment.impl.model.input.Event;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class EventPickerTest {

    @Test
    public void testPickingAllEvents() {
        try (
                InputStream inputStream = getClass().getResourceAsStream("happy-path-input.json-stream")
        ) {
            ParserConfig parserConfig = ConfigFactory.createConfig(inputStream);
            ConditionChecker conditionChecker = new ConditionChecker(10, Duration.ofSeconds(30), System.nanoTime());
            EventPicker eventPicker = new EventPicker(parserConfig, conditionChecker);
            Event event1 = eventPicker.next();
            Event event2 = eventPicker.next();
            Event event3 = eventPicker.next();

            assertNotNull(event1);
            assertNotNull(event2);
            assertNotNull(event3);
            assertFalse(eventPicker.hasNext());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPickingNotExistingElement() {
        try (
                InputStream inputStream = getClass().getResourceAsStream("happy-path-input.json-stream")
        ) {
            ParserConfig parserConfig = ConfigFactory.createConfig(inputStream);
            ConditionChecker conditionChecker = new ConditionChecker(10, Duration.ofSeconds(30), System.nanoTime());
            EventPicker eventPicker = new EventPicker(parserConfig, conditionChecker);
            Event event1 = eventPicker.next();
            Event event2 = eventPicker.next();
            Event event3 = eventPicker.next();

            assertNotNull(event1);
            assertNotNull(event2);
            assertNotNull(event3);

            Assertions.assertThrows(NoSuchElementException.class, eventPicker::next);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
