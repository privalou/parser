package tech.picnic.assignment.impl;

import org.junit.jupiter.api.Test;
import tech.picnic.assignment.impl.model.input.Article;
import tech.picnic.assignment.impl.model.input.Event;
import tech.picnic.assignment.impl.model.input.Picker;
import tech.picnic.assignment.impl.model.input.TemperatureZone;
import tech.picnic.assignment.impl.model.output.OutputArticle;
import tech.picnic.assignment.impl.model.output.OutputPicker;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class OutputGeneratorTest {

    @Test
    public void testBuildingOutput() {
        OutputGenerator outputGenerator = new OutputGenerator();

        Article article = new Article();
        article.setId("3");
        article.setName("Article");
        article.setTemperatureZone(TemperatureZone.AMBIENT);

        Picker picker = new Picker();
        picker.setId("2");
        picker.setActiveSince("2018-09-20T08:20:00Z");
        picker.setName("Picker");

        Event event = new Event();
        event.setId("1");
        event.setArticle(article);
        event.setPicker(picker);
        event.setQuantity(2);
        event.setTimestamp("2018-12-20T11:51:00Z");

        List<OutputPicker> processedList = outputGenerator.process(Collections.singletonList(event));

        assertEquals(1, processedList.size());

        OutputPicker outputPicker = processedList.get(0);
        assertNotNull(outputPicker);
        assertEquals("2018-09-20T08:20:00Z", outputPicker.getActiveSince());
        assertEquals("Picker", outputPicker.getPickerName());

        Set<OutputArticle> picks = outputPicker.getPicks();
        assertEquals(1, picks.size());

        OutputArticle outputArticle = picks.iterator().next();
        assertNotNull(outputArticle);
        assertEquals("2018-12-20T11:51:00Z", outputArticle.getTimestamp());
        assertEquals("Article".toUpperCase(), outputArticle.getArticleName());
    }
}
