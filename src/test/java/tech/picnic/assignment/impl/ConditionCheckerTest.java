package tech.picnic.assignment.impl;

import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConditionCheckerTest {

    @Test
    public void testPassingConditions() {
        ConditionChecker conditionChecker = new ConditionChecker(10, Duration.ofSeconds(30), System.nanoTime());

        boolean checkMaximumAmountOfEvents = conditionChecker.checkIfOutOfConditions(9);

        assertFalse(checkMaximumAmountOfEvents);
    }

    @Test
    public void testOutOfDurationFailing() {

        ConditionChecker conditionChecker = new ConditionChecker(10,
                Duration.ofMillis(10),
                System.currentTimeMillis()-11);

        boolean checkOutOfTimeCondition = conditionChecker.checkIfOutOfConditions(9);

        assertTrue(checkOutOfTimeCondition);
    }

    @Test
    public void testMaximumAndExceededNumberOfEvents() {
        ConditionChecker conditionChecker = new ConditionChecker(10, Duration.ofSeconds(30), System.nanoTime());

        boolean checkMaximumAmountOfEvents = conditionChecker.checkIfOutOfConditions(10);
        boolean checkExceededAmountOfEvents = conditionChecker.checkIfOutOfConditions(11);

        assertTrue(checkMaximumAmountOfEvents);
        assertTrue(checkExceededAmountOfEvents);
    }
}
